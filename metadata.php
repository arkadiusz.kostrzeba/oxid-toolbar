<?php

use Adsr\ToolBar\Extended\Core\ToolBarUtilsView;
use Adsr\ToolBar\Extended\Core\ToolBarViewConfig;
use Adsr\ToolBar\Demo\Controllers\DemoController;
use OxidEsales\Eshop\Core\UtilsView;
use OxidEsales\Eshop\Core\ViewConfig;


$sMetadataVersion = '2.0';

$aModule = array(
    'id' => 'toolbar',
    'title' => 'toolbar',
    'description' => 'Debug ToolBar for OXID',
    'version' => '1.0.0',
    'author' => 'Arkadiusz Kostrzeba',
    'thumbnail' => 'logo_black.png',
    'url' => '',
    'email' => 'arkadiusz.kostrzeba@gmail.com',
    'extend' => [
        ViewConfig::class => ToolBarViewConfig::class,
        UtilsView::class => ToolBarUtilsView::class,
    ],
    'controllers' => [
        'adsrDemo' => DemoController::class,
    ],
    'templates' => [
        'adsrDemo' => 'adsr/toolbar/Demo/Views/adsrDemo.tpl'
    ],
    'blocks' => [
        [
            'template' => 'layout/base.tpl',
            'block' => 'base_style',
            'file' => 'toolbar_head_js',
        ]
    ],
    'events' => array(
        'onActivate' => '',
        'onDeactivate' => '',
    ),
);
