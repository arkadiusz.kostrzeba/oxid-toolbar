<?php
/**
 * Copyright (c) 2018.  Arkadiusz Kostrzeba arkadiusz.kostrzeba@gmail.com
 */

/**
 * Created by PhpStorm.
 * User: 4dsr
 * Date: 2018-07-20
 * Time: 02:51
 */

namespace Adsr\ToolBar\Smarty;

use Adsr\ToolBar\Profiler\Profile;


class SmartyProfile extends Profile
{
    /** @var array
     * Used to build template tree during rendering
     */
    protected $nodeQueue;


    protected $template;


    public function __construct($name, $type = self::ROOT)
    {
        $this->nodeQueue = [];
        $this->template = $name;
        parent::__construct($name, $type);
    }


    public function enterChild($name = null, $type = null)
    {
        if($this->isRoot() && $name && $type) {
            $profile           = new SmartyProfile($name, $type);
            $this->nodeQueue[] = $profile;
        }
    }


    public function leaveChild()
    {
        $child = array_pop($this->nodeQueue);
        $currentNode = end($this->nodeQueue);
        $child->leave();

        if($currentNode){
            $currentNode->addProfile($child);
        } else {
            $this->profiles[] = $child;

            if($this->isRoot()){
                // write leave data also for ROOT profile
                $this->leave();
            }
        }
    }

    public function getName()
    {
        return $this->name;
    }

    public function getTemplate()
    {
        return $this->template;
    }

    public function isTemplate()
    {
        return $this->type === 'fetch' ||  $this->type === '_smarty_include';
    }

    public function isInclude()
    {
        return $this->type === '_smarty_include';
    }

    public function isBlock()
    {
        return $this->type === 'block';
    }

    public function isCapture()
    {
        return $this->type === 'capture';
    }

}
