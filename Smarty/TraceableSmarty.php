<?php
/**
 * Copyright (c) 2018.  Arkadiusz Kostrzeba arkadiusz.kostrzeba@gmail.com
 */

/**
 * Created by PhpStorm.
 * User: 4dsr
 * Date: 2018-07-22
 * Time: 13:35
 */

namespace Adsr\ToolBar\Smarty;


class TraceableSmarty extends \Smarty
{
    protected $traceConfig;
    protected $profile;

    /**
     * TraceableSmarty constructor.
     * @param \stdClass $traceConfig
     */
    public function __construct(\stdClass $traceConfig)
    {
        $this->traceConfig = $traceConfig;
        $this->profile = new SmartyProfile('Smarty');
        parent::__construct();
    }

    public function fetch($resource_name, $cache_id = null, $compile_id = null, $display = false)
    {
        if($this->traceConfig->enableTplLocator){
            dump(__FUNCTION__ .  " $resource_name");
        }

        if($this->traceConfig->enableProfiler){
            $this->profile->enterChild($resource_name, __FUNCTION__);
            $output = parent::fetch($resource_name, $cache_id, $compile_id, $display);
            $this->profile->leaveChild();

            return $output;
        }

        $output = parent::fetch($resource_name, $cache_id, $compile_id, $display);

        return $output;
    }

    public function _smarty_include($params)
    {
        if($this->traceConfig->enableTplLocator){
            dump(__FUNCTION__ .  " {$params['smarty_include_tpl_file']}");
        }

        if($this->traceConfig->enableProfiler){
            $this->profile->enterChild($params['smarty_include_tpl_file'], __FUNCTION__);
            parent::_smarty_include($params);
            $this->profile->leaveChild();
            return;
        }

        parent::_smarty_include($params);
    }

    public function getProfile()
    {
        return $this->profile;
    }


    /** Experimental */
    public function traceCall($method, $args)
    {
        $returnValue = call_user_func_array(array(__CLASS__, "parent::{$method}"), $args);

        return $returnValue;
    }

    public function getRenderedTemplates()
    {
        // $this->profile->leave();

       //dump($this->profile);
        return [];
    }
}