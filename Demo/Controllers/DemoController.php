<?php
/**
 * Created by PhpStorm.
 * User: 4dsr
 * Date: 2018-07-10
 * Time: 21:23
 */

namespace Adsr\ToolBar\Demo\Controllers;


use Adsr\ToolBar\DebugBar;
use OxidEsales\Eshop\Application\Controller\FrontendController;
use OxidEsales\Eshop\Core\Exception\StandardException;
use OxidEsales\Eshop\Core\Registry;

class DemoController extends FrontendController
{
    protected $_sThisTemplate = 'adsrDemo';

    public function init()
    {
        parent::init();

        try{
            $this->throwException();

        } catch (StandardException $oEx) {
            $debugBar = DebugBar::getInstance();
            $debugBar['exceptions']->addException($oEx);
        }
    }

    public function render()
    {
        $debugBar = DebugBar::getInstance();
        $debugBar['messages']->info('Logged Info');
        $debugBar['time']->measure('My long operation', function() {
            sleep(1);
        });
        return parent::render();
    }
    protected function throwException()
    {
        $oException = new StandardException('We will pass this exception to DebugBar');
        throw $oException;
    }
}