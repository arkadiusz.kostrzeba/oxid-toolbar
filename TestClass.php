<?php
/**
 * Created by PhpStorm.
 * User: 4dsr
 * Date: 2018-06-28
 * Time: 00:10
 */

namespace Adsr\ToolBar;

use DebugBar\StandardDebugBar;

class TestClass
{
    public function __construct()
    {
        $debugbar = new StandardDebugBar();
        $debugbarRenderer = $debugbar->getJavascriptRenderer();

        $debugbar["messages"]->addMessage("hello world!");
    }
}