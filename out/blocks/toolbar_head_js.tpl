[{$smarty.block.parent}]
[{assign var=debugBarRenderer value=$oViewConf->getDebugBarRenderer()}]
<style>
    [{$debugBarRenderer->dumpCssAssets()}]
</style>
<script>
    [{$debugBarRenderer->dumpJsAssets()}]
</script>
