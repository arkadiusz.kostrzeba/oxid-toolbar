<?php
/**
 * Copyright (c) 2018.  Arkadiusz Kostrzeba arkadiusz.kostrzeba@gmail.com
 */

/**
 * Created by PhpStorm.
 * User: 4dsr
 * Date: 2018-07-24
 * Time: 00:22
 */

namespace Adsr\ToolBar\Profiler;


interface ProfilerInterface
{
    /**
     * Starts measurement. Saves initial conditions to local properties
     * @return void
     */
    public function enter();

    /**
     * Ends measurement. Saves initial condition sto local properties
     * @return void
     */
    public function leave();

    /**
     * Removes collected data
     * @return void
     */
    public function reset();

    /**
     * Returns the duration in microseconds.
     * @return void
     */
    public function getDuration();

}