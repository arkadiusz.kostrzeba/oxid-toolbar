<?php
/**
 * Copyright (c) 2018.  Arkadiusz Kostrzeba arkadiusz.kostrzeba@gmail.com
 */

namespace Adsr\ToolBar\Profiler;


class Profile implements \IteratorAggregate, \Serializable, ProfilerInterface
{
    const ROOT = 'ROOT';

    protected $type;
    protected $name;
    protected $starts = array();

    protected $ends = array();
    protected $profiles = array();


    public function __construct($name, $type = self::ROOT)
    {
        $this->name = $name;
        $this->type = $type;
        $this->enter();

    }

    public function isRoot()
    {
        return self::ROOT === $this->type;
    }

    public function getProfiles()
    {
        return $this->profiles;
    }

    public function addProfile(Profile $profile)
    {
        $this->profiles[] = $profile;
    }

    /**

     *
     * @return int
     */
    public function getDuration()
    {
        if ($this->isRoot() && $this->profiles) {
            // for the root node with children, duration is the sum of all child durations
            $duration = 0;
            foreach ($this->profiles as $profile) {
                $duration += $profile->getDuration();
            }

            return $duration;
        }

        return isset($this->ends['wt']) && isset($this->starts['wt']) ? $this->ends['wt'] - $this->starts['wt'] : 0;
    }

    /**
     * Returns the memory usage in bytes.
     *
     * @return int
     */
    public function getMemoryUsage()
    {
        return isset($this->ends['mu']) && isset($this->starts['mu']) ? $this->ends['mu'] - $this->starts['mu'] : 0;
    }

    /**
     * Returns the peak memory usage in bytes.
     *
     * @return int
     */
    public function getPeakMemoryUsage()
    {
        return isset($this->ends['pmu']) && isset($this->starts['pmu']) ? $this->ends['pmu'] - $this->starts['pmu'] : 0;
    }

    /**
     * Starts the profiling.
     */
    public function enter()
    {
        $this->starts = array(
            'wt' => microtime(true),
            'mu' => memory_get_usage(),
            'pmu' => memory_get_peak_usage(),
        );
    }

    /**
     * Stops the profiling.
     */
    public function leave()
    {
        $this->ends = array(
            'wt' => microtime(true),
            'mu' => memory_get_usage(),
            'pmu' => memory_get_peak_usage(),
        );
    }

    public function reset()
    {
        $this->starts = $this->ends = $this->profiles = array();
        $this->enter();
    }

    public function getIterator()
    {
        return new \ArrayIterator($this->profiles);
    }

    public function serialize()
    {
        return serialize(array($this->template, $this->name, $this->type, $this->starts, $this->ends, $this->profiles));
    }

    public function unserialize($data)
    {
        list($this->template, $this->name, $this->type, $this->starts, $this->ends, $this->profiles) = unserialize($data);
    }
}
