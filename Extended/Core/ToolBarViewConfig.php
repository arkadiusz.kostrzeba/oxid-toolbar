<?php
/**
 * Copyright (c) 2018.  Arkadiusz Kostrzeba arkadiusz.kostrzeba@gmail.com
 */

/**
 * Created by PhpStorm.
 * User: 4dsr
 * Date: 2018-06-28
 * Time: 00:25
 */
namespace Adsr\ToolBar\Extended\Core;


use Adsr\ToolBar\DebugBar;

class ToolBarViewConfig extends ToolBarViewConfig_parent
{
    protected $debugBar;

    /**
     * ToolBarViewConfig constructor.
     * @throws \DebugBar\DebugBarException
     * @throws \OxidEsales\Eshop\Core\Exception\DatabaseConnectionException
     */
    public function __construct()
    {
        $this->debugBar = DebugBar::getInstance();
    }

    public function getDebugBarRenderer()
    {
        return $this->debugBar->getJavascriptRenderer();
    }

    public function getModulePath()
    {

    }
}