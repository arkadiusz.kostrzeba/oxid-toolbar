<?php
/**
 * Copyright (c) 2018.  Arkadiusz Kostrzeba arkadiusz.kostrzeba@gmail.com
 */

/**
 * Created by PhpStorm.
 * User: 4dsr
 * Date: 2018-07-20
 * Time: 00:34
 */

namespace Adsr\ToolBar\Extended\Core;

use Adsr\ToolBar\DebugBar;
use Adsr\ToolBar\Smarty\TraceableSmarty;


class ToolBarUtilsView extends ToolBarUtilsView_parent
{
    /**
     * @param bool $blReload
     * @return TraceableSmarty
     * @throws \DebugBar\DebugBarException
     * @throws \OxidEsales\Eshop\Core\Exception\DatabaseConnectionException
     */
    public function getSmarty($blReload = false)
    {
        $smartyTraceConfig = DebugBar::getInstance()->getSmartyTraceConfig();

        return $this->getTraceableSmarty($smartyTraceConfig, $blReload);
    }

    /**
     * Copy of parent::getSmarty method. Smarty object is replaced with TraceableSmarty object
     *
     * @param \stdClass $smartyTraceConfig
     * @param bool $blReload
     * @return TraceableSmarty
     */
    protected function getTraceableSmarty(\stdClass $smartyTraceConfig, $blReload = false)
    {
        if (!self::$_oSmarty || $blReload) {
            $this->_aTemplateDir = [];
            self::$_oSmarty = new TraceableSmarty($smartyTraceConfig);
            $this->_fillCommonSmartyProperties(self::$_oSmarty);
            $this->_smartyCompileCheck(self::$_oSmarty);
        }

        return self::$_oSmarty;
    }
}