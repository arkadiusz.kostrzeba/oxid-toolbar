<?php
/**
 * Copyright © OXID eSales AG. All rights reserved.
 * See LICENSE file for license details.
 */

require_once dirname(__FILE__) . "/bootstrap.php";
/**
 * Redirect to Setup, if shop is not configured
 */
redirectIfShopNotConfigured();

$clientIp = $_SERVER['REMOTE_ADDR'];
$allowDebugFrom = ['89.70.198.204'];

$debug = false;
if(class_exists('\Adsr\ToolBar\DebugBar') && in_array($clientIp, $allowDebugFrom)){
    // debugBar initialization
    $debugBar = \Adsr\ToolBar\DebugBar::getInstance();
    $debug = true;
}

//Starts the shop
OxidEsales\EshopCommunity\Core\Oxid::run();

if($debug){
    // render debugBar and collected data
    echo $debugBar->getJavascriptRenderer()->render();
}
