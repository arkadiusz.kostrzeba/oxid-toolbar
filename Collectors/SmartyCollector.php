<?php
/**
 * User: 4dsr
 * Date: 2018-07-16
 * Time: 22:30
 */

namespace Adsr\ToolBar\Collectors;

use Adsr\ToolBar\Smarty\SmartyProfile;
use Adsr\Toolbar\Smarty\TraceableSmarty;
use DebugBar\DataCollector\AssetProvider;
use DebugBar\DataCollector\DataCollector;
use DebugBar\DataCollector\Renderable;
use OxidEsales\Eshop\Core\Registry;

class SmartyCollector extends DataCollector implements Renderable, AssetProvider

{
    /** @var TraceableSmarty  */
    protected $smarty;
    protected $templateCount;
    protected $includeCount;
    protected $blockCount;
    protected $captureCount;
    protected $profile;
    protected $templates;

    public function __construct(TraceableSmarty $smarty, $config)
    {
        $this->smarty = $smarty;
        $this->profile     = $smarty->getProfile();
        $this->setXdebugLinkTemplate('xdebugLink:%f:%l', true); //TODO: set correct link template
    }

    public function getWidgets()
    {
        return array(
            'smarty' => array(
                'icon' => 'leaf',
                'widget' => 'PhpDebugBar.Widgets.SmartyTemplatesWidget',
                'map' => 'smarty',
                'default' => json_encode(array('templates' => array())),
            ),
            'smarty:badge' => array(
                'map' => 'smarty.badge',
                'default' => 0
            )
        );
    }

    public function getAssets()
    {
        $basePath = Registry::getConfig()->getModulesDir() . 'adsr/toolbar/out/';
        return array(
            'base_path' => $basePath,
//            'base_url' => $baseUrl,
            'css' => 'css/smartyWidget.css',
            'js' => 'js/smartyWidget.js'
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'smarty';
    }

    /**
     * Called by the DebugBar when data needs to be collected
     *
     * @return array Collected data
     */
    function collect()
    {
        $this->templateCount = $this->includeCount = $this->blockCount = $this->captureCount = 0;
        $this->templates     = array();
        $this->computeData($this->profile);

        return array(
            'nb_templates'                => $this->templateCount,
            'nb_blocks'                   => $this->blockCount,
            'nb_macros'                   => $this->captureCount,
            'templates'                   => $this->templates,
            'accumulated_render_time'     => $this->profile->getDuration(),
            'accumulated_render_time_str' => $this->getDataFormatter()->formatDuration($this->profile->getDuration()),
            'memory_usage_str'            => $this->getDataFormatter()->formatBytes($this->profile->getMemoryUsage()),
            'callgraph'                   => $this->getHtmlCallGraph(),
            'badge'                       => implode(
                '/',
                array(
                    $this->templateCount,
                    $this->blockCount,
                    $this->captureCount,
                )
            ),
        );
    }

    public function getHtmlCallGraph()
    {
        $dumper = new \Twig_Profiler_Dumper_Html();

        //return $dumper->dump($this->profile);
    }

    public function getXdebugLink($template, $line = 1)
    {
        if (is_null($this->smarty)) {
            return null;
        }
        $file = $this->smarty->template_dir[0] . $template;

        return parent::getXdebugLink($file, $line);
    }

    private function computeData(SmartyProfile $profile)
    {
        $this->templateCount += ($profile->isTemplate() ? 1 : 0);
        $this->includeCount += ($profile->isInclude() ? 1 : 0);
        $this->blockCount    += ($profile->isBlock() ? 1 : 0);
        $this->captureCount    += ($profile->isCapture() ? 1 : 0);
        if ($profile->isTemplate()) {
            $this->templates[] = array(
                'name'            => $profile->getName(),
                'render_time'     => $profile->getDuration(),
                'render_time_str' => $this->getDataFormatter()->formatDuration($profile->getDuration()),
                'memory_str'      => $this->getDataFormatter()->formatBytes($profile->getMemoryUsage()),
                'xdebug_link'     => $this->getXdebugLink($profile->getTemplate()),
            );
        }
        foreach ($profile as $p) {
            $this->computeData($p);
        }
    }
}