<?php
/**
 * Created by PhpStorm.
 * User: 4dsr
 * Date: 2018-07-16
 * Time: 22:30
 */

namespace Adsr\ToolBar\Collectors;

use DebugBar\DataCollector\AssetProvider;
use DebugBar\DataCollector\DataCollector;
use DebugBar\DataCollector\Renderable;
use OxidEsales\Eshop\Core\Module\ModuleList;

class ModuleCollector extends DataCollector implements Renderable, AssetProvider

{

    public function getWidgets()
    {
        return array(
            "modules" => array(
                "icon" => "cogs",
                "widget" => 'PhpDebugBar.Widgets.KVListWidget',
                "tooltip" => "Modules List",
                "map" => "modules.templates",
                "default" => "['No modules data']"
            )
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'modules';
    }


    /**
     * Returns an array with the following keys:
     *  - base_path
     *  - base_url
     *  - css: an array of filenames
     *  - js: an array of filenames
     *  - inline_css: an array map of content ID to inline CSS content (not including <style> tag)
     *  - inline_js: an array map of content ID to inline JS content (not including <script> tag)
     *  - inline_head: an array map of content ID to arbitrary inline HTML content (typically
     *        <style>/<script> tags); it must be embedded within the <head> element
     *
     * All keys are optional.
     *
     * Ideally, you should store static assets in filenames that are returned via the normal css/js
     * keys.  However, the inline asset elements are useful when integrating with 3rd-party
     * libraries that require static assets that are only available in an inline format.
     *
     * The inline content arrays require special string array keys:  the caller of this function
     * will use them to deduplicate content.  This is particularly useful if multiple instances of
     * the same asset provider are used.  Inline assets from all collectors are merged together into
     * the same array, so these content IDs effectively deduplicate the inline assets.
     *
     * @return array
     */
    function getAssets()
    {
        // TODO: Implement getAssets() method.
    }

    /**
     * Called by the DebugBar when data needs to be collected
     *
     * @return array Collected data
     */
    function collect()
    {
        $oModuleList = oxNew(ModuleList::class);
        $aModulesConfig = $oModuleList->getModules();

        $chains = $oModuleList->parseModuleChains($aModulesConfig);
        $versions = $oModuleList->getModuleConfigParametersByKey(ModuleList::MODULE_KEY_VERSIONS);
        $extensions = $oModuleList->getModuleConfigParametersByKey(ModuleList::MODULE_KEY_EXTENSIONS);
        $controllers = $oModuleList->getModuleConfigParametersByKey(ModuleList::MODULE_KEY_CONTROLLERS);
        $files = $oModuleList->getModuleConfigParametersByKey(ModuleList::MODULE_KEY_FILES);
        $path = $oModuleList->getModuleConfigParametersByKey(ModuleList::MODULE_KEY_PATHS);
        $templates = $oModuleList->getModuleConfigParametersByKey(ModuleList::MODULE_KEY_TEMPLATES);
        $events = $oModuleList->getModuleConfigParametersByKey(ModuleList::MODULE_KEY_EVENTS);

        return array(
            'chains' => ['12', '33'],
            'templates' => ['mall' => 'asasasa', 'aaaa' => 'asdad']
        );
    }


}