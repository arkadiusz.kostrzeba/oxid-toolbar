<?php

namespace Adsr\ToolBar;

use Adsr\ToolBar\Collectors\ModuleCollector;
use Adsr\ToolBar\Collectors\SmartyCollector;
use DebugBar\StandardDebugBar;
use DebugBar\DebugBarException;
use OxidEsales\Eshop\Core\Database\Adapter\Doctrine\Database;
use OxidEsales\Eshop\Core\DatabaseProvider;
use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\UtilsView;

class DebugBar extends StandardDebugBar
{
    /** @var self */
    protected static $instance;

    /** @var \stdClass */
    protected $smartyTraceConfig;

    /**
     * DebugBar constructor.
     * @throws DebugBarException
     * @throws \OxidEsales\Eshop\Core\Exception\DatabaseConnectionException
     */
    public function __construct()
    {
        if(self::$instance !== null){
            throw new DebugBarException(__CLASS__ . ' is a singleton. Use getInstance instead.');
        }

        $this->initSmartyConfig();

        parent::__construct();
    }

    /**
     * @return DebugBar|StandardDebugBar
     * @throws \DebugBar\DebugBarException
     * @throws \OxidEsales\Eshop\Core\Exception\DatabaseConnectionException
     */
    public static function getInstance()
    {
        if(self::$instance === null){
            self::$instance = new self();
            self::$instance->registerSmartyCollector();
            self::$instance->registerDebugBarDoctrineCollector();
            self::$instance->addCollector(new ModuleCollector());
        }

        return self::$instance;
    }

    /**
     * @throws \DebugBar\DebugBarException
     * @throws \OxidEsales\Eshop\Core\Exception\DatabaseConnectionException
     */
    public function registerDebugBarDoctrineCollector()
    {
        /** @var Database $db */
        $db = DatabaseProvider::getDb();
        $debugStack = new \Doctrine\DBAL\Logging\DebugStack();
        $db->getConnection()->getConfiguration()->setSQLLogger($debugStack);
        $this->addCollector(new \DebugBar\Bridge\DoctrineCollector($debugStack));
    }

    public function registerSmartyCollector()
    {
        $oUtilsView = Registry::get(UtilsView::class);
        $this->addCollector(new SmartyCollector($oUtilsView->getSmarty()));
    }

    public function getSmartyTraceConfig()
    {
        if(!$this->smartyTraceConfig){
            $this->initSmartyConfig();
        }

        return $this->smartyTraceConfig;
    }

    protected function initSmartyConfig()
    {
        $smartyConfig = new \stdClass();
        $smartyConfig->enableProfiler = true;
        $smartyConfig->enableTplLocator = false;

        $this->smartyTraceConfig = $smartyConfig;
    }
}